package dataBase;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class DataBase {
    private String url;
    private String user;
    private String password;

    private Connection con;
    private Statement stmt;

    public DataBase(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public void connect() throws SQLException {
        con = DriverManager.getConnection(url, user, password);
        stmt = con.createStatement();
    }

    public void close() throws SQLException {
        stmt.close();
        con.close();
    }

    public ResultSet request(String query) throws SQLException {
        return stmt.executeQuery(query);
    }

    public void requestForData(String query) throws SQLException {
        stmt.executeUpdate(query);
    }

    public int countColumns(String tableName) throws SQLException {
        ResultSetMetaData rsm = request("SELECT * FROM `" + tableName + "`").getMetaData();
        return rsm.getColumnCount();
    }

    public ArrayList<String> getColumnNames(String tableName, int columnCount) throws SQLException {
        ArrayList<String> columnsNames = new ArrayList<>();
        ResultSetMetaData rsm = request("SELECT * FROM `" + tableName + "`").getMetaData();
        for (int i = 1; i <= columnCount; i++) {
            columnsNames.add(rsm.getColumnName(i));
        }

        return columnsNames;
    }

    public ObservableList<HashMap> getInfo(String tableName, int columnCount, ArrayList<String> columnsNames) throws SQLException {
        return FXCollections.observableArrayList(readData(tableName, columnCount, columnsNames));
    }

    private ArrayList<HashMap> readData(String tableName, int columnCount, ArrayList<String> columnNames) throws SQLException {
        ArrayList<HashMap> hashMaps = new ArrayList<>();
        ResultSet rs = request("SELECT * FROM `" + tableName + "`");
        while (rs.next()) {
            HashMap<String, String> dataBaseInfo = new HashMap<>();
            for (int i = 1; i <= columnCount; i++) {
                dataBaseInfo.put(columnNames.get(i - 1), rs.getString(i));
            }
            hashMaps.add(dataBaseInfo);
        }
        rs.close();

        return hashMaps;
    }

    public void addColumn(String tableName, String columnName, String columnType) throws SQLException {
        columnName = columnName.replaceAll("\\s+", "_");
        ResultSetMetaData rsm = request("SELECT * FROM `" + tableName + "`").getMetaData();
        requestForData("ALTER TABLE `" + tableName + "` ADD COLUMN `" + columnName + "` " + columnType + " AFTER `" + rsm.getColumnName(countColumns(tableName)) + "`;");
    }

    public void addRow(String tableName, ArrayList<String> values) throws SQLException {
        ResultSetMetaData rsm = request("SELECT * FROM `" + tableName + "`").getMetaData();
        ArrayList<String> columnNames = getColumnNames(tableName, rsm.getColumnCount());

        String query = "INSERT INTO `" + tableName + "` (" +
                genColumsNames(columnNames) + ") VALUES (" +
                genValues(values) + ");";
        requestForData(query);
    }

    private String genColumsNames(ArrayList<String> columnNames) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < columnNames.size(); i++) {
            if (i != columnNames.size() - 1) {
                stringBuilder.append("`").append(columnNames.get(i)).append("`, ");

            } else {
                stringBuilder.append("`").append(columnNames.get(i)).append("`");
            }
        }
        return stringBuilder.toString();
    }

    private String genValues(ArrayList<String> values) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < values.size(); i++) {

            if (i != values.size() - 1) {
                stringBuilder.append("'").append(values.get(i)).append("', ");

            } else {
                stringBuilder.append("'").append(values.get(i)).append("'");
            }
        }

        return stringBuilder.toString();
    }

    public void removeRow(String tableName, int numRow) throws SQLException {
        requestForData("DELETE FROM `" + tableName + "` WHERE №='" + numRow + "';");
    }

    public void removeColumn(String tableName, String columnName) throws SQLException {
        requestForData("ALTER TABLE `" + tableName + "` DROP COLUMN `" + columnName + "`;");
    }

    public void changeCell(String tableName, String columnName, String value, int numRow) throws SQLException {
        requestForData("UPDATE `" + tableName + "` SET " + columnName + "='" + value + "' WHERE №='" + numRow + "';");
    }

    public void changeRow(String tableName, ArrayList<String> columnName, ArrayList<String> value, int numRow) throws SQLException {
        for (int i = 1; i < columnName.size(); i++) {
            requestForData("UPDATE `" + tableName + "` SET " + columnName.get(i) + "='" + value.get(i) + "' WHERE №='" + numRow + "';");
        }

        requestForData("UPDATE `" + tableName + "` SET " + columnName.get(0) + "='" + value.get(0) + "' WHERE №='" + numRow + "';");
    }

    public void renameColumn(String tableName, String nameColumn, String newNameColumn) throws SQLException {
        requestForData("ALTER TABLE `" + tableName + "` CHANGE `" + nameColumn + "` `" + newNameColumn + "` INT(5);");
    }
}

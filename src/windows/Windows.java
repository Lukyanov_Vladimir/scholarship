package windows;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;

public class Windows {
    public static void launchDataBaseEditorWindow(Window prevWindow) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml\\dataBaseEditor.fxml"));
        Parent root = fxmlLoader.load();
        Stage bdStage = new Stage();
        bdStage.setResizable(false);
        bdStage.setTitle("Редактор базы данных");
        bdStage.initModality(Modality.WINDOW_MODAL);
        bdStage.initOwner(prevWindow);
        bdStage.setScene(new Scene(root));
        bdStage.show();
    }

    public static void launchResultWindow(Window prevWindow) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml\\stipendWindow.fxml"));
        Parent root = fxmlLoader.load();
        Stage delRowStage = new Stage();
        delRowStage.setResizable(false);
        delRowStage.setTitle("Стипендия");
        delRowStage.initModality(Modality.WINDOW_MODAL);
        delRowStage.initOwner(prevWindow);
        delRowStage.setScene(new Scene(root));
        delRowStage.show();
    }

    public static void launchAddRecordWindow(Window prevWindow) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml\\addRecord.fxml"));
        Parent root = fxmlLoader.load();
        Stage addRecord = new Stage();
        addRecord.setResizable(false);
        addRecord.setTitle("Добавление записи");
        addRecord.initModality(Modality.WINDOW_MODAL);
        addRecord.initOwner(prevWindow);
        addRecord.setScene(new Scene(root));
        addRecord.show();
    }

    public static void launchRemoveRecordWindow(Window prevWindow) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml\\removeRecord.fxml"));
        Parent root = fxmlLoader.load();
        Stage removeRecord = new Stage();
        removeRecord.setResizable(false);
        removeRecord.setTitle("Удаление записи");
        removeRecord.initModality(Modality.WINDOW_MODAL);
        removeRecord.initOwner(prevWindow);
        removeRecord.setScene(new Scene(root));
        removeRecord.show();
    }

    public static void launchAddColumnWindow(Window prevWindow) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml\\addColumn.fxml"));
        Parent root = fxmlLoader.load();
        Stage addColumn = new Stage();
        addColumn.setResizable(false);
        addColumn.setTitle("Добавление колонки");
        addColumn.initModality(Modality.WINDOW_MODAL);
        addColumn.initOwner(prevWindow);
        addColumn.setScene(new Scene(root));
        addColumn.show();
    }

    public static void launchRemoveColumnWindow(Window prevWindow) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml\\removeColumn.fxml"));
        Parent root = fxmlLoader.load();
        Stage removeColumn = new Stage();
        removeColumn.setResizable(false);
        removeColumn.setTitle("Удаление колонки");
        removeColumn.initModality(Modality.WINDOW_MODAL);
        removeColumn.initOwner(prevWindow);
        removeColumn.setScene(new Scene(root));
        removeColumn.show();
    }

    public static void launchChangeValueRecordWindow(Window prevWindow) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml\\changeValueRecord.fxml"));
        Parent root = fxmlLoader.load();
        Stage changeValueRecord = new Stage();
        changeValueRecord.setResizable(false);
        changeValueRecord.setTitle("Изменение ячейки");
        changeValueRecord.initModality(Modality.WINDOW_MODAL);
        changeValueRecord.initOwner(prevWindow);
        changeValueRecord.setScene(new Scene(root));
        changeValueRecord.show();
    }

    public static void launchChangeRecordWindow(Window prevWindow) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml\\changeRecord.fxml"));
        Parent root = fxmlLoader.load();
        Stage changeRecord = new Stage();
        changeRecord.setResizable(false);
        changeRecord.setTitle("Редактирование записи");
        changeRecord.initModality(Modality.WINDOW_MODAL);
        changeRecord.initOwner(prevWindow);
        changeRecord.setScene(new Scene(root));
        changeRecord.show();
    }

    public static void launchRenameColumnWindow(Window prevWindow) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Windows.class.getResource("/fxml\\renameColumn.fxml"));
        Parent root = fxmlLoader.load();
        Stage renameColumn = new Stage();
        renameColumn.setResizable(false);
        renameColumn.setTitle("Изменение названия колонки");
        renameColumn.initModality(Modality.WINDOW_MODAL);
        renameColumn.initOwner(prevWindow);
        renameColumn.setScene(new Scene(root));
        renameColumn.show();
    }
}

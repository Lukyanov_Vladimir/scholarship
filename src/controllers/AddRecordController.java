package controllers;

import constants.Constants;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.sql.SQLException;
import java.util.ArrayList;

public class AddRecordController implements Constants{

    @FXML
    private VBox vBox;

    private ArrayList<TextField> textFields;

    @FXML
    private void initialize() {
        textFields = new ArrayList<>();
        Button addBtn = new Button("Добавить");
        addBtn.setFont(Font.font(17));

        try {
            DATA_BASE.connect();
            generateForm();
            DATA_BASE.close();
            vBox.getChildren().add(addBtn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        addBtn.setOnAction(actionEvent -> {
            try {
                DATA_BASE.connect();
                DATA_BASE.addRow(TABLE_NAME, getValues());
                DATA_BASE.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void generateForm() throws SQLException {
        ArrayList<String> columnNames = DATA_BASE.getColumnNames(TABLE_NAME, DATA_BASE.countColumns(TABLE_NAME));
        ArrayList<Label> labels = createLabels(columnNames);
        createTextFields(columnNames.size());

        for (int i = 0; i < columnNames.size(); i++) {
            vBox.getChildren().add(labels.get(i));
            vBox.getChildren().add(textFields.get(i));
        }
    }

    private ArrayList<Label> createLabels(ArrayList<String> columnNames) {
        ArrayList<Label> labels = new ArrayList<>();
        for (String columnName : columnNames) {
            Label label = new Label();
            label.setText(columnName);
            label.setTextAlignment(TextAlignment.LEFT);
            label.setFont(Font.font(17));
            labels.add(label);
        }

        return labels;
    }

    private void createTextFields(int columsCount) {
        for (int i = 0; i < columsCount; i++) {
            TextField textField = new TextField();
            textField.setAlignment(Pos.CENTER);
            textFields.add(textField);
        }
    }

    private ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();

        for (TextField textField : textFields) {
            values.add(textField.getText());
        }

        return values;
    }
}

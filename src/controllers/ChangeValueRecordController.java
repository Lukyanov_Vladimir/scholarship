package controllers;

import constants.Constants;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.sql.SQLException;
import java.util.ArrayList;

public class ChangeValueRecordController implements Constants{

    @FXML
    private ComboBox<String> columnName;

    @FXML
    private TextField numRecord;

    @FXML
    private TextField newValue;

    @FXML
    private Button changeBtn;

    @FXML
    private void initialize() {
         try {
            ArrayList<String> columsNames = getColumnsNames();
            columsNames = removeSpacings(columsNames);
            columnName.setValue(columsNames.get(0));
            columnName.setItems(FXCollections.observableArrayList(columsNames));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        changeBtn.setOnAction(actionEvent -> {
            try {
                DATA_BASE.connect();
                DATA_BASE.changeCell(TABLE_NAME, addSpacings(columnName.getValue()), newValue.getText(), Integer.parseInt(numRecord.getText()));
                DATA_BASE.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private ArrayList<String> getColumnsNames() throws SQLException {
        DATA_BASE.connect();
        ArrayList<String> columnNames = DATA_BASE.getColumnNames(TABLE_NAME, DATA_BASE.countColumns(TABLE_NAME));
        DATA_BASE.close();
        return columnNames;
    }

    private ArrayList<String> removeSpacings(ArrayList<String> arrayList) {
        ArrayList<String> noSpacings = new ArrayList<>();
        for (String elem : arrayList) {
            noSpacings.add(elem.replaceAll("_+", " "));
        }
        return noSpacings;
    }

    private String addSpacings(String str) {
        return str.replaceAll("\\s+", "_");
    }

}


package controllers;

import constants.Constants;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import stipend.Stipend;
import student.Student;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StipendWindowController implements Constants{

    @FXML
    private TableView<Student> tableView;

    @FXML
    private TableColumn<Student, Integer> numColumn;

    @FXML
    private TableColumn<Student, String> columnName;

    @FXML
    private TableColumn<Student, Integer> stipendColumn;

    @FXML
    private void initialize() {
        numColumn.setStyle("-fx-alignment: CENTER;");
        columnName.setStyle("-fx-alignment: CENTER-LEFT;");
        stipendColumn.setStyle("-fx-alignment: CENTER;");

        try {
            DATA_BASE.connect();
            int columnCount = columnCount();
            ArrayList<Student> students = createStudents(columnCount);
            DATA_BASE.close();
            calcStipendStudents(students);
            tableView.setItems(createObservableListStudents(students));
            setValuesColumns();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private int columnCount() throws SQLException {
        return DATA_BASE.countColumns(TABLE_NAME);
    }

    private ArrayList<Student> createStudents(int columnCount) throws SQLException {
        ArrayList<Student> students = new ArrayList<>();

        ResultSet rs = DATA_BASE.request("SELECT * FROM `" + TABLE_NAME+"`;");
        while (rs.next()) {
            int number = rs.getInt(1);
            String name = rs.getString(2);
            ArrayList<Integer> grades = new ArrayList<>();

            for (int i = 3; i <= columnCount; i++) {
                grades.add(rs.getInt(i));
            }

            students.add(new Student(number, name, 0, grades));
        }
        rs.close();

        return students;
    }

    private void calcStipendStudents(ArrayList<Student> students) {
        Stipend stipend = new Stipend(890);

        for (Student student : students) {
            student.setStipend(stipend.calc(student.getGrades()));
        }
    }

    private ObservableList<Student> createObservableListStudents(ArrayList<Student> students) {
        return FXCollections.observableArrayList(students);
    }

    private void setValuesColumns() {
        numColumn.setCellValueFactory(new PropertyValueFactory<>("number"));
        columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        stipendColumn.setCellValueFactory(new PropertyValueFactory<>("stipend"));
    }
}
package controllers;

import constants.Constants;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.MapValueFactory;
import windows.Windows;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class DataBaseEditorController implements Constants {

    @FXML
    private Button addRecordBtn;

    @FXML
    private Button removeRecordBtn;

    @FXML
    private Button changeRecordBtn;

    @FXML
    private Button addColumnBtn;

    @FXML
    private Button removeColumnBtn;

    @FXML
    private Button renameColumnBtn;

    @FXML
    private Button changeValueRecordBtn;

    @FXML
    private Button updateBtn;

    @FXML
    private TableView<HashMap> tableView;

    private int columnCount;

    @FXML
    private void initialize() {
        try {
            showDataBaseInfo();
        } catch (Exception e) {
            e.printStackTrace();
        }

        addRecordBtn.setOnAction(actionEvent -> {
            try {
                Windows.launchAddRecordWindow(addRecordBtn.getScene().getWindow());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        updateBtn.setOnAction(actionEvent -> {
            tableView.getColumns().clear();
            try {
                showDataBaseInfo();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        removeRecordBtn.setOnAction(actionEvent -> {
            try {
                Windows.launchRemoveRecordWindow(removeRecordBtn.getScene().getWindow());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        addColumnBtn.setOnAction(actionEvent -> {
            try {
                Windows.launchAddColumnWindow(addColumnBtn.getScene().getWindow());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        removeColumnBtn.setOnAction(actionEvent -> {
            try {
                Windows.launchRemoveColumnWindow(removeColumnBtn.getScene().getWindow());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        changeValueRecordBtn.setOnAction(actionEvent -> {
            try {
                Windows.launchChangeValueRecordWindow(changeValueRecordBtn.getScene().getWindow());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        changeRecordBtn.setOnAction(actionEvent -> {
            try {
                Windows.launchChangeRecordWindow(changeRecordBtn.getScene().getWindow());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        renameColumnBtn.setOnAction(actionEvent -> {
            try {
                Windows.launchRenameColumnWindow(renameColumnBtn.getScene().getWindow());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void showDataBaseInfo() throws SQLException {
        DATA_BASE.connect();

        columnCount = DATA_BASE.countColumns(TABLE_NAME);

        ArrayList<String> columnNames = removeSpacing(DATA_BASE.getColumnNames(TABLE_NAME, columnCount));
        ArrayList<TableColumn> tableColumns = createTableColumns(columnNames);

        setItemsInTable(columnNames);
        setValuesColumns(tableColumns, columnNames);
        addColumnsInTable(tableColumns);

        DATA_BASE.close();
    }

    private ArrayList<TableColumn> createTableColumns(ArrayList<String> columnNames) {
        ArrayList<TableColumn> tableColumns = new ArrayList<>();
        for (int i = 0; i < columnCount; i++) {
            if (i == 1) {
                TableColumn tableColumn = new TableColumn(columnNames.get(i));
                tableColumn.setStyle("-fx-alignment: CENTER-LEFT;");
                tableColumns.add(tableColumn);

            } else {
                TableColumn tableColumn = new TableColumn(columnNames.get(i));
                tableColumn.setStyle("-fx-alignment: CENTER;");
                tableColumns.add(tableColumn);
            }
        }
        return tableColumns;
    }

    private void setItemsInTable(ArrayList<String> columnNames) throws SQLException {
        tableView.setItems(DATA_BASE.getInfo(TABLE_NAME, columnCount, columnNames));
    }

    private void addColumnsInTable(ArrayList<TableColumn> tableColumns) {
        for (int i = 0; i < columnCount; i++) {
            tableView.getColumns().add(tableColumns.get(i));
        }
    }

    private void setValuesColumns(ArrayList<TableColumn> tableColumns, ArrayList<String> columnNames) {
        for (int i = 0; i < columnCount; i++) {
            tableColumns.get(i).setCellValueFactory(new MapValueFactory<String>(columnNames.get(i)));
        }
    }

    private ArrayList<String> removeSpacing(ArrayList<String> arrayList) {
        ArrayList<String> noSpacings = new ArrayList<>();
        for (String elem : arrayList) {
            noSpacings.add(elem.replaceAll("_+", " "));
        }
        return noSpacings;
    }
}
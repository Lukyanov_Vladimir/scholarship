package controllers;

import constants.Constants;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class RemoveRecordController implements Constants{

    @FXML
    private TextField numRecord;

    @FXML
    private Button removeBtn;

    @FXML
    private void initialize() {
        removeBtn.setOnAction(actionEvent -> {
            try {
                DATA_BASE.connect();
                DATA_BASE.removeRow(TABLE_NAME, Integer.parseInt(numRecord.getText()));
                DATA_BASE.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }
}

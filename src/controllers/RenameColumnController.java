package controllers;

import constants.Constants;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.sql.SQLException;
import java.util.ArrayList;

public class RenameColumnController implements Constants{

    @FXML
    private ComboBox<String> columnName;

    @FXML
    private TextField newColumnName;

    @FXML
    private Button renameBtn;

    @FXML
    private void initialize() {
        try {
            DATA_BASE.connect();
            ArrayList<String> columnNames = DATA_BASE.getColumnNames(TABLE_NAME, DATA_BASE.countColumns(TABLE_NAME));
            DATA_BASE.close();
            columnNames.remove("№");
            columnNames.remove("ФИО");
            columnNames = removeSpacings(columnNames);
            columnName.setValue(columnNames.get(0));
            columnName.setItems(FXCollections.observableArrayList(columnNames));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        renameBtn.setOnAction(actionEvent -> {
            try {
                DATA_BASE.connect();
                DATA_BASE.renameColumn(TABLE_NAME, addSpacings(columnName.getValue()), removeSpacings(newColumnName.getText()));
                DATA_BASE.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private ArrayList<String> removeSpacings(ArrayList<String> arrayList) {
        ArrayList<String> noSpacings = new ArrayList<>();
        for (String elem : arrayList) {
            noSpacings.add(elem.replaceAll("_+", " "));
        }
        return noSpacings;
    }

    private String removeSpacings(String str) {
        return str.replaceAll("\\s+", "_");
    }

    private String addSpacings(String str) {
        return str.replaceAll("\\s+", "_");
    }
}


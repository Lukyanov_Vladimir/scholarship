package controllers;

import constants.Constants;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import windows.Windows;

import java.io.IOException;

public class MainStageController {

    @FXML
    private Button dataBaseEditorBtn;

    @FXML
    private Button stipendCalcBtn;

    @FXML
    private Button exitBtn;

    @FXML
    private void initialize() {
        dataBaseEditorBtn.setOnAction(actionEvent -> {
            try {
                Windows.launchDataBaseEditorWindow(dataBaseEditorBtn.getScene().getWindow());
            } catch (IOException e) {
            }
        });

        stipendCalcBtn.setOnAction(actionEvent -> {
            try {
                Windows.launchResultWindow(stipendCalcBtn.getScene().getWindow());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        exitBtn.setOnAction(actionEvent -> {
            Stage parentStage = (Stage) exitBtn.getScene().getWindow();
            parentStage.close();
        });
    }
}
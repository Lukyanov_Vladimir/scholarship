package controllers;

import constants.Constants;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class AddColumnController implements Constants{

    @FXML
    private TextField name;

    @FXML
    private Button addBtn;

    @FXML
    private void initialize() {
        addBtn.setOnAction(actionEvent -> {
            try {
                DATA_BASE.connect();
                DATA_BASE.addColumn(TABLE_NAME, name.getText(), "INT(5)");
                DATA_BASE.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }
}


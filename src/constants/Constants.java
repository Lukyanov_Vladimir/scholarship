package constants;

import dataBase.DataBase;

public interface Constants {
    DataBase DATA_BASE = new DataBase("jdbc:mysql://localhost/student_performance?serverTimezone=Europe/Moscow&useSSL=false", "root", null);
    String TABLE_NAME = "students_info";
}

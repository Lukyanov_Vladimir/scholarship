package student;

import java.util.ArrayList;

public class Student {
    private int number;
    private String name;
    private double stipend;
    private ArrayList<Integer> grades;

    public Student(int number, String name, double stipend, ArrayList<Integer> grades) {
        this.number = number;
        this.name = name;
        this.stipend = stipend;
        this.grades = grades;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public double getStipend() {
        return stipend;
    }

    public ArrayList<Integer> getGrades() {
        return grades;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStipend(double stipend) {
        this.stipend = stipend;
    }

    public void setGrades(ArrayList<Integer> grades) {
        this.grades = grades;
    }
}

package stipend;

import java.util.ArrayList;
import java.util.HashMap;

public class Stipend {
    private final double minStipend;
    private int numAllGrades;
    private HashMap<Integer, Integer> numGrades;

    public Stipend(double minStipend) {
        this.minStipend = minStipend;

        numGrades = new HashMap<>();
    }

    public double calc(ArrayList<Integer> grades) {
        numAllGrades = grades.size();

        countGrades(grades);

        double result = calcStipend(0);

        if (!checkTripleOrDeuce()) {
            if (checkAllFives()) {
                result = calcStipend(75);

            } else if (checkFivesMoreThanFours()) {
                result = calcStipend(50);

            } else if (checkFivesEqualFours()) {
                result = calcStipend(25);

            }

        } else {
            result = 0;
        }

        return result;
    }

    private void countGrades(ArrayList<Integer> grades) {
        int tempGrade = 2;
        int countGrade = 0;
        for (int i = 0; i < 4; i++) {
            for (int grade : grades) {
                if (grade == tempGrade)
                    countGrade++;
            }
            numGrades.put(tempGrade, countGrade);
            countGrade = 0;
            tempGrade++;
        }
    }

    public boolean checkTripleOrDeuce() {
        boolean check = false;

        if (numGrades.get(2) != 0)
            check = true;

        if (numGrades.get(3) != 0)
            check = true;

        return check;
    }

    public boolean checkFivesEqualFours() {
        return numGrades.get(5).equals(numGrades.get(4));
    }

    public boolean checkFivesMoreThanFours() {
        return numGrades.get(5) > numGrades.get(4);
    }

    public boolean checkAllFives() {
        return numGrades.get(5) == numAllGrades;
    }

    public double calcStipend(int bonusPercent) {
        double stipend = minStipend;

        if (bonusPercent != 0) {
            stipend += minStipend * bonusPercent / 100;
        }

        return stipend;
    }
}

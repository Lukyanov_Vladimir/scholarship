package stipend;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class StipendTest {
    @Test
    public void testforZero() {
        ArrayList<Integer> grades = new ArrayList<>();
        grades.add(0);
        grades.add(0);
        grades.add(0);
        grades.add(0);
        grades.add(0);

        Stipend stipend = new Stipend(890);
        double result = stipend.calc(grades);

        //Assert.assertEquals(1557.5, result, 0);
    }

    @Test
    public void testNegativeNumbers() {
        ArrayList<Integer> grades = new ArrayList<>();
        grades.add(-5);
        grades.add(-5);
        grades.add(-5);
        grades.add(-5);
        grades.add(-5);

        Stipend stipend = new Stipend(890);
        double result = stipend.calc(grades);

        //Assert.assertEquals(1557.5, result, 0);
    }

    @Test
    public void testCheckLackOfTripleOrDeuce() {
        ArrayList<Integer> grades = new ArrayList<>();
        grades.add(5);
        grades.add(5);
        grades.add(5);
        grades.add(5);
        grades.add(5);

        Stipend stipend = new Stipend(890);
        stipend.calc(grades);

        boolean result = stipend.checkTripleOrDeuce();

        Assert.assertFalse(result);
    }

    @Test
    public void testCheckTriple() {
        ArrayList<Integer> grades = new ArrayList<>();
        grades.add(5);
        grades.add(3);
        grades.add(5);
        grades.add(5);
        grades.add(5);

        Stipend stipend = new Stipend(890);
        stipend.calc(grades);

        boolean result = stipend.checkTripleOrDeuce();

        Assert.assertTrue(result);
    }

    @Test
    public void testCheckDeuce() {
        ArrayList<Integer> grades = new ArrayList<>();
        grades.add(5);
        grades.add(5);
        grades.add(5);
        grades.add(2);
        grades.add(5);

        Stipend stipend = new Stipend(890);
        stipend.calc(grades);

        boolean result = stipend.checkTripleOrDeuce();

        Assert.assertTrue(result);
    }

    @Test
    public void testCheckAllFives() {
        ArrayList<Integer> grades = new ArrayList<>();
        grades.add(5);
        grades.add(5);
        grades.add(5);
        grades.add(5);
        grades.add(5);

        Stipend stipend = new Stipend(890);
        stipend.calc(grades);

        boolean result = stipend.checkAllFives();

        Assert.assertTrue(result);
    }

    @Test
    public void testCheckFivesMoreThanFours() {
        ArrayList<Integer> grades = new ArrayList<>();
        grades.add(5);
        grades.add(5);
        grades.add(4);
        grades.add(4);
        grades.add(5);

        Stipend stipend = new Stipend(890);
        stipend.calc(grades);

        boolean result = stipend.checkFivesMoreThanFours();

        Assert.assertTrue(result);
    }

    @Test
    public void testCheckFivesEqualFours() {
        ArrayList<Integer> grades = new ArrayList<>();
        grades.add(5);
        grades.add(5);
        grades.add(4);
        grades.add(4);
        grades.add(5);
        grades.add(4);

        Stipend stipend = new Stipend(890);
        stipend.calc(grades);

        boolean result = stipend.checkFivesEqualFours();

        Assert.assertTrue(result);
    }

    @Test
    public void testCalcStipend() {
        ArrayList<Integer> grades = new ArrayList<>();
        grades.add(5);
        grades.add(5);
        grades.add(4);
        grades.add(4);
        grades.add(3);
        grades.add(4);

        Stipend stipend = new Stipend(890);

        double result1 = stipend.calcStipend(0);
        Assert.assertEquals(890, result1, 0);

        double result2 = stipend.calcStipend(75);
        Assert.assertEquals(1557.5, result2, 0);

        double result3 = stipend.calcStipend(50);
        Assert.assertEquals(1335, result3, 0);

        double result4 = stipend.calcStipend(25);
        Assert.assertEquals(1112.5, result4, 0);

        double result5 = stipend.calc(grades);
        Assert.assertEquals(0, result5, 0);
    }
}